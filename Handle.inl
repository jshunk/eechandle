#include "Handle.h"

template< typename DataType >
THandle< DataType >::THandle( DataType* pMemory ) 
	: m_pMemory( pMemory )	
{
}

template< typename DataType >
ptr	THandle< DataType >::GetKey()
{ 
	return reinterpret_cast< ptr >( m_pMemory );
}
