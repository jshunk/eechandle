#pragma once

#include "Types.h"

class CHandle
{
public:
	virtual			~CHandle() {};
	virtual void	Disable() = 0;
	virtual ptr		GetKey() = 0;
};

template< typename DataType >
class THandle : public CHandle
{
public:
					THandle() : m_pMemory( NULL )	{}
					THandle( DataType* pMemory );
	DataType*		Get()							{ return m_pMemory; }
	virtual void	Disable()						{ m_pMemory = NULL; }
	void			Set( DataType* pMemory )		{ m_pMemory = pMemory; }
	virtual ptr		GetKey();

private:
	DataType*	m_pMemory;
};
